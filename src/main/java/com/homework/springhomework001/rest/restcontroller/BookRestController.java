package com.homework.springhomework001.rest.restcontroller;

import com.homework.springhomework001.repository.dto.BookDto;
import com.homework.springhomework001.rest.request.BookRequestModel;
import com.homework.springhomework001.rest.response.BaseApiResponse;
import com.homework.springhomework001.service.impl.BookServiceImplement;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class BookRestController {

    private BookServiceImplement bookService;
    @Autowired
    public BookRestController(BookServiceImplement bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> findAll() {
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        List<BookDto> bookDto1 = bookService.findAll();
        List<BookRequestModel> request = new ArrayList<>();
        for(BookDto bookDto2: bookDto1){
            request.add(modelMapper.map(bookDto2,BookRequestModel.class));
        }

        response.setMessage("All record has found.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(@RequestBody BookDto book) {
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();

        BookDto bookDto = bookService.selectLastID();
        bookService.insert(book);
        BookRequestModel request = mapper.map(bookDto,BookRequestModel.class);

        response.setMessage("Record has saved successfully.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> findOne(@PathVariable int id){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = bookService.findOne(id);
        BookRequestModel request = mapper.map(bookDto, BookRequestModel.class);

        response.setMessage("Record has found successfully.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> updateById(
            @PathVariable int id, @RequestBody BookDto book
    ){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        bookService.updateById(id, book);
        BookDto bookDto = bookService.findOne(id);
        BookRequestModel request = mapper.map(bookDto,BookRequestModel.class);

        response.setMessage("Record has updated successfully.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> deleteById(@PathVariable int id){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = bookService.findOne(id);

        BookRequestModel request = mapper.map(bookDto,BookRequestModel.class);
        response.setMessage("Record has removed successfully.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        bookService.deleteById(id);
        return  ResponseEntity.ok(response);
    }







}
package com.homework.springhomework001.rest.restcontroller;

import com.homework.springhomework001.repository.dto.CategoryDto;
import com.homework.springhomework001.rest.request.CategoryRequestModel;
import com.homework.springhomework001.rest.response.BaseApiResponse;
import com.homework.springhomework001.service.impl.CategoryServiceImplement;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class CategoryRestController {

    private CategoryServiceImplement categoryService;

    @Autowired
    public void setCategoryService(CategoryServiceImplement categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> findAll() {

        BaseApiResponse<List<CategoryRequestModel>> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();

        List<CategoryDto> categoryDtoList = categoryService.findAll();
        List<CategoryRequestModel> categories = new ArrayList<>();

        for (CategoryDto categoryDto : categoryDtoList) {
            categories.add(mapper.map(categoryDto, CategoryRequestModel.class));
        }

        response.setMessage("All record has found.");
        response.setData(categories);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @PostMapping("/categories")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(
            @RequestBody CategoryRequestModel category) {

        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        CategoryDto categoryDto = modelMapper.map(category, CategoryDto.class);

        UUID uuid = UUID.randomUUID();
        categoryDto.setCategoryId(uuid.toString());
        CategoryDto result = categoryService.insert(categoryDto);
        CategoryRequestModel result2 = modelMapper.map(result, CategoryRequestModel.class);

        response.setMessage("Record has saved successfully.");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> findOne(@PathVariable int id){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        CategoryDto categoryDto = categoryService.findById(id);
        CategoryRequestModel request = mapper.map(categoryDto, CategoryRequestModel.class);

        response.setMessage("Record has found successfully.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> deleteById(@PathVariable int id){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        CategoryDto categoryDto = categoryService.findById(id);

        CategoryRequestModel request = mapper.map(categoryDto,CategoryRequestModel.class);
        response.setMessage("Record has removed successfully.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        categoryService.deleteById(id);

        return  ResponseEntity.ok(response);
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> updateById(
            @PathVariable int id, @RequestBody CategoryDto category
    ){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        categoryService.updateById(id, category);
        CategoryDto categoryDto = categoryService.findById(id);
        CategoryRequestModel request = mapper.map(categoryDto,CategoryRequestModel.class);

        response.setMessage("Record has updated successfully.");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

}

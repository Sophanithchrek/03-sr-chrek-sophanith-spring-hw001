package com.homework.springhomework001.rest.request;

public class CategoryRequestModel {

    private String title;

    public CategoryRequestModel() {}

    public CategoryRequestModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

package com.homework.springhomework001.service;

import com.homework.springhomework001.repository.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> findAll();
    CategoryDto insert(CategoryDto category);
    CategoryDto findById(int id);
    boolean deleteById(int id);
    boolean updateById(int id, CategoryDto category);

}

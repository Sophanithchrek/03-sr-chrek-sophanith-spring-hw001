package com.homework.springhomework001.service.impl;

import com.homework.springhomework001.repository.BookRepository;
import com.homework.springhomework001.repository.dto.BookDto;
import com.homework.springhomework001.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImplement implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookDto> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public BookDto selectLastID() {
        return bookRepository.selectLastID();
    }

    @Override
    public BookDto insert(BookDto book) {
        boolean isInserted = bookRepository.insert(book);
        return isInserted ? book : null;
    }

    @Override
    public BookDto findOne(int id) {
        return bookRepository.findOne(id);
    }

    @Override
    public boolean updateById(int id, BookDto book) {
        return bookRepository.updateById(id, book);
    }

    @Override
    public boolean deleteById(int id) {
        return bookRepository.deleteById(id);
    }
}

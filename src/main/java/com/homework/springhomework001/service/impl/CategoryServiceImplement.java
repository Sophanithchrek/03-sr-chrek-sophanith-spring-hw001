package com.homework.springhomework001.service.impl;

import com.homework.springhomework001.repository.CategoryRepository;
import com.homework.springhomework001.repository.dto.CategoryDto;
import com.homework.springhomework001.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplement implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryDto> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public CategoryDto insert(CategoryDto article) {
        boolean isInserted = categoryRepository.insert(article);
        if (isInserted) {
            return article;
        } else {
            return null;
        }
    }

    @Override
    public CategoryDto findById(int id) {
        return categoryRepository.findById(id);
    }

    @Override
    public boolean deleteById(int id) {
        return categoryRepository.deleteById(id);
    }

    @Override
    public boolean updateById(int id, CategoryDto category) {
        return categoryRepository.updateById(id, category);
    }
}

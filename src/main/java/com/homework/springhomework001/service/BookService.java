package com.homework.springhomework001.service;

import com.homework.springhomework001.repository.dto.BookDto;

import java.util.List;

public interface BookService {

    List<BookDto> findAll();
    BookDto insert(BookDto bookDto);
    BookDto selectLastID();
    BookDto findOne(int id);
    boolean deleteById(int id);
    boolean updateById(int id, BookDto book);

}

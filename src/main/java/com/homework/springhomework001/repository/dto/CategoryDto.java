package com.homework.springhomework001.repository.dto;

public class CategoryDto {
    private int id;
    private String categoryId;
    private String title;

    public CategoryDto() {}

    public CategoryDto(int id, String categoryId, String title) {
        this.id = id;
        this.categoryId = categoryId;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "id=" + id +
                ", categoryId='" + categoryId + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}

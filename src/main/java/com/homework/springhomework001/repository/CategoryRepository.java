package com.homework.springhomework001.repository;

import com.homework.springhomework001.repository.dto.CategoryDto;
import com.homework.springhomework001.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("SELECT * FROM categories;")
    List<CategoryDto> findAll();

    @Insert("INSERT INTO categories(category_id, title) VALUES(#{categoryId}, #{title});")
    boolean insert(CategoryDto article);

    // ---------------------------------------------------- //

    @DeleteProvider(type = CategoryProvider.class, method="deleteById")
    boolean deleteById(int id);

    @SelectProvider(type = CategoryProvider.class, method = "findOne")
    CategoryDto findById(int id);

    @Select("SELECT id, title FROM tb_categories ORDER BY id DESC LIMIT 1")
    CategoryDto selectLastID();

    @UpdateProvider(type = CategoryProvider.class, method = "updateById")
    boolean updateById(int id, @Param(value = "category") CategoryDto category);


}

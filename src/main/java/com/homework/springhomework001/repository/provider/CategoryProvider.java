package com.homework.springhomework001.repository.provider;

import com.homework.springhomework001.repository.dto.CategoryDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String deleteById(int id){
        return new SQL(){{
            DELETE_FROM("categories");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String findOne(int id){
        return new SQL(){{
            SELECT("id, title");
            FROM("categories");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String updateById(int id, @Param("category") CategoryDto category){
        return new SQL(){{
            UPDATE("categories");
            SET("title = #{category.title}");
            WHERE("id = "+id);
        }}.toString();
    }
}

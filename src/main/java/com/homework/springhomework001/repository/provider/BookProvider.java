package com.homework.springhomework001.repository.provider;

import com.homework.springhomework001.repository.dto.BookDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String findOne(int id){
        return new SQL(){{
            SELECT("books.id, books.title, books.author, books.description, books.thumbnail, categories.id as cid, categories.title as ctitle");
            FROM("books");
            INNER_JOIN("categories ON books.category_id = categories.id");
            WHERE("books.id = #{id}");
        }}.toString();
    }

    public String updateById(int id, @Param("book") BookDto book){
        return new SQL(){{
            UPDATE("books");
            SET("title = #{book.title}");
            SET("author = #{book.author}");
            SET("description = #{book.description}");
            SET("thumbnail = #{book.thumbnail}");
            SET("category_id = #{book.category_id}");
            WHERE("id = "+id);
        }}.toString();
    }

    public String deleteById(int id){
        return new SQL(){{
            DELETE_FROM("books");
            WHERE("id = #{id}");
        }}.toString();
    }
}

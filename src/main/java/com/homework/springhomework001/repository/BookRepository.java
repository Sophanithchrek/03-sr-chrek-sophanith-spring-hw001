package com.homework.springhomework001.repository;

import com.homework.springhomework001.repository.dto.BookDto;
import com.homework.springhomework001.repository.provider.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    @Insert("INSERT INTO books(title, author, description, thumbnail, category_id) VALUES(#{title}, #{author}, #{description}, #{thumbnail}, #{category_id});")
    boolean insert(BookDto bookDto);

    @Select("SELECT books.id, books.title, books.author, books.description, books.thumbnail, categories.id as cid, categories.category_id as categoryid, categories.title as ctitle FROM books INNER JOIN categories ON books.category_id = categories.id ORDER BY books.id DESC LIMIT 1;")
    @Results(value = {
            @Result(property = "category.id", column = "cid"),
            @Result(property = "category.categoryId", column = "categoryid"),
            @Result(property = "category.title", column = "ctitle"),
    })
    BookDto selectLastID();

    @Select("SELECT books.id, books.title, books.author, books.description, books.thumbnail, categories.id as cid, categories.category_id as categoryid, categories.title as ctitle FROM books INNER JOIN categories ON books.category_id = categories.id;")
    @Results(value = {
            @Result(property = "category.id", column = "cid"),
            @Result(property = "category.categoryId", column = "categoryid"),
            @Result(property = "category.title", column = "ctitle"),
    })
    List<BookDto> findAll();

    @SelectProvider(type = BookProvider.class, method = "findOne")
    @Results(value = {
            @Result(property = "category.id", column = "cid"),
            @Result(property = "category.title", column = "ctitle"),
    })
    BookDto findOne(int id);

    @UpdateProvider(type = BookProvider.class, method = "updateById")
    boolean updateById(int id, @Param(value = "book") BookDto book);

    @DeleteProvider(type = BookProvider.class, method="deleteById")
    boolean deleteById(int id);

}
